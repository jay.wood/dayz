<VehicleParts> gglaw_bus
	SparkPlug
	CarBattery
	CarRadiator
	HeadlightH7
	HeadlightH7
	BusPsycho_driver
	BusPsychoWheel_offroad
	BusPsychoWheel_offroad
	BusPsychoWheel_offroad
	BusPsychoWheel_offroad
	
<VehicleParts> 	CrSk_Mitsubishi_Lancer_Evo_X
	SparkPlug
	CarBattery
	CarRadiator
	HeadlightH7
	HeadlightH7
	LancerEvoX_bagazhnik
	LancerEvoX_dver_1_1
	LancerEvoX_dver_1_2
	LancerEvoX_dver_2_1
	LancerEvoX_dver_2_2
	LancerEvoX_kapot
	LancerEvoX_koleso
	LancerEvoX_koleso
	LancerEvoX_koleso
	LancerEvoX_koleso
	LancerEvoX_koleso
	LancerEvoX_korpus1
	LancerEvoX_korpus2

<VehicleParts> CrSk_Nissan_Skyline_GTR_BNR34	
	SparkPlug
	CarBattery
	CarRadiator
	HeadlightH7
	HeadlightH7
	SkylineBNR34_dver_1_1
	SkylineBNR34_dver_2_1
	SkylineBNR34_kapot
	SkylineBNR34_bagazhnik
	SkylineBNR34_koleso
	SkylineBNR34_koleso
	SkylineBNR34_koleso
	SkylineBNR34_koleso
	
<VehicleParts> CrSk_Ford_Bronco_Offroad_86	
	SparkPlug
	CarBattery
	CarRadiator
	HeadlightH7
	HeadlightH7
	CanisterGasoline
	BroncoOff86_dver_1_1
	BroncoOff86_dver_2_1
	BroncoOff86_kapot
	BroncoOff86_koleso
	BroncoOff86_koleso
	BroncoOff86_koleso
	BroncoOff86_koleso
	BroncoOff86_koleso
		
<VehicleParts> CrSk_Chevrolet_Tahoe_08
	SparkPlug
	CarBattery
	CarRadiator
	HeadlightH7
	HeadlightH7
	CanisterGasoline
	ChevyTahoe08_bagazhnik
	ChevyTahoe08_kapot
	ChevyTahoe08_dver_1_1
	ChevyTahoe08_dver_1_2
	ChevyTahoe08_dver_2_1
	ChevyTahoe08_dver_2_2
	ChevyTahoe08_koleso
	ChevyTahoe08_koleso
	ChevyTahoe08_koleso
	ChevyTahoe08_koleso
	ChevyTahoe08_koleso	
	
<VehicleParts> CrSk_Chevrolet_Tahoe_08_Police
	SparkPlug
	CarBattery
	CarRadiator
	HeadlightH7
	HeadlightH7
	CanisterGasoline
	ChevyTahoe08_bagazhnik
	ChevyTahoe08_kapot
	ChevyTahoe08_dver_1_1_Police
	ChevyTahoe08_dver_2_1_Police
	ChevyTahoe08_dver_2_2
	ChevyTahoe08_dver_1_2
	ChevyTahoe08_koleso
	ChevyTahoe08_koleso
	ChevyTahoe08_koleso
	ChevyTahoe08_koleso
	ChevyTahoe08_koleso		
	
<VehicleParts> gglaw_f150Psycho
	SparkPlug
	CarBattery
	CarRadiator
	HeadlightH7
	HeadlightH7      
	f150PsychoWheel_offroad
	f150PsychoWheel_offroad
	f150PsychoWheel_offroad
	f150PsychoWheel_offroad
	f150PsychoWheel_offroad
	f150Psycho_driver
	f150Psycho_codriver
	f150Psycho_cargo1
	f150Psycho_cargo2
	f150Psycho_hood
	f150Psycho_trunk    

<VehicleParts> gglaw_f150Psycho_black
	SparkPlug
	CarBattery
	CarRadiator
	HeadlightH7
	HeadlightH7      
	f150PsychoWheel_offroad
	f150PsychoWheel_offroad
	f150PsychoWheel_offroad
	f150PsychoWheel_offroad
	f150PsychoWheel_offroad
	f150Psycho_driver_black
	f150Psycho_codriver_black
	f150Psycho_cargo1_black
	f150Psycho_cargo2_black
	f150Psycho_hood_black
	f150Psycho_trunk_black    
    
<VehicleParts> gglaw_f150Psycho_pink
	SparkPlug
	CarBattery
	CarRadiator
	HeadlightH7
	HeadlightH7      
	f150PsychoWheel_offroad
	f150PsychoWheel_offroad
	f150PsychoWheel_offroad
	f150PsychoWheel_offroad
	f150PsychoWheel_offroad
	f150Psycho_driver_red
	f150Psycho_codriver_red
	f150Psycho_cargo1_red
	f150Psycho_cargo2_red
	f150Psycho_hood_red
	f150Psycho_trunk_red    
    
<VehicleParts> gglaw_f150Psycho_green
	SparkPlug
	CarBattery
	CarRadiator
	HeadlightH7
	HeadlightH7      
	f150PsychoWheel_offroad
	f150PsychoWheel_offroad
	f150PsychoWheel_offroad
	f150PsychoWheel_offroad
	f150PsychoWheel_offroad
	f150Psycho_driver_green
	f150Psycho_codriver_green
	f150Psycho_cargo1_green
	f150Psycho_cargo2_green
	f150Psycho_hood_green
	f150Psycho_trunk_green   

<VehicleParts> GGlaw_Buggy_black
	SparkPlug
	CarBattery
	CarRadiator
	Headlighth7
	Headlighth7
	BuggyPsychoWheel_offroad
	BuggyPsychoWheel_offroad
	BuggyPsychoWheelDouble_offroad	
	BuggyPsychoWheelDouble_offroad	

<VehicleParts> GGlaw_Buggy
	SparkPlug
	CarBattery
	CarRadiator
	Headlighth7
	Headlighth7
	BuggyPsychoWheel_offroad
	BuggyPsychoWheel_offroad
	BuggyPsychoWheelDouble_offroad	
	BuggyPsychoWheelDouble_offroad	

<VehicleParts> GGlaw_Buggy_pink
	SparkPlug
	CarBattery
	CarRadiator
	Headlighth7
	Headlighth7
	BuggyPsychoWheel_offroad
	BuggyPsychoWheel_offroad
	BuggyPsychoWheelDouble_offroad	
	BuggyPsychoWheelDouble_offroad	

<VehicleParts> GGlaw_Buggy_green
	SparkPlug
	CarBattery
	CarRadiator
	Headlighth7
	Headlighth7	
	BuggyPsychoWheel_offroad
	BuggyPsychoWheel_offroad
	BuggyPsychoWheelDouble_offroad	
	BuggyPsychoWheelDouble_offroad	
	
<VehicleParts> GGlaw_amc
    SparkPlug
    CarBattery
    CarRadiator
    Headlighth7
    Headlighth7
    AMCPsycho_driver
    AMCPsycho_codriver
    AMCPsycho_hood
    AMCPsycho_trunk
    AMCPsychoWheel_offroad
    AMCPsychoWheel_offroad
    AMCPsychoWheel_offroad
    AMCPsychoWheel_offroad

<VehicleParts> GGlaw_amc_black
    SparkPlug
    CarBattery
    CarRadiator
    Headlighth7
    Headlighth7
    AMCPsycho_driver_black
    AMCPsycho_codriver_black
    AMCPsycho_hood_black
    AMCPsycho_trunk_black
    AMCPsychoWheel_offroad
    AMCPsychoWheel_offroad
    AMCPsychoWheel_offroad
    AMCPsychoWheel_offroad

<VehicleParts> GGlaw_amc_pink
    SparkPlug
    CarBattery
    CarRadiator
    Headlighth7
    Headlighth7
    AMCPsycho_driver
    AMCPsycho_codriver
    AMCPsycho_hood
    AMCPsycho_trunk
    AMCPsychoWheel_offroad
    AMCPsychoWheel_offroad
    AMCPsychoWheel_offroad
    AMCPsychoWheel_offroad
	
<VehicleParts> GGlaw_amc_red
    SparkPlug
    CarBattery
    CarRadiator
    Headlighth7
    Headlighth7
    AMCPsycho_driver
    AMCPsycho_codriver
    AMCPsycho_hood
    AMCPsycho_trunk
    AMCPsychoWheel_offroad
    AMCPsychoWheel_offroad
    AMCPsychoWheel_offroad
    AMCPsychoWheel_offroad

<VehicleParts> GGlaw_amc_green
    SparkPlug
    CarBattery
    CarRadiator
    Headlighth7
    Headlighth7
    AMCPsycho_driver_black
    AMCPsycho_codriver_black
    AMCPsycho_hood_black
    AMCPsycho_trunk_black
    AMCPsychoWheel_offroad
    AMCPsychoWheel_offroad
    AMCPsychoWheel_offroad
    AMCPsychoWheel_offroad

<VehicleParts> GGlaw_amc_pink
	SparkPlug
	CarBattery
	CarRadiator
	Headlighth7
	Headlighth7
	AMCPsycho_driver_pink
	AMCPsycho_codriver_pink
	AMCPsycho_hood_pink
	AMCPsycho_trunk_pink
	AMCPsychoWheel_offroad
	AMCPsychoWheel_offroad
	AMCPsychoWheel_offroad
	AMCPsychoWheel_offroad