///////////////////////////////////////////////////////////
//                 LOOT UPDATES READ ALL 				 //
///////////////////////////////////////////////////////////

I have provided loot updates in the folders [LOOT]

The folders are named for what builds you may have from me.
Remember Dis_OilRig is not a free download so this will only apply to a serveral server owners.

The 4 folders provided contain different loot setup's relevant to how your server is running with my mods.

so: If you only run vanilla ChernarusPlus: this is: 

mpmissions/dayzOffline.Cheraruplus/ Then you will only need the file matching your server.
mpmissions/Expansion.ChernarusPlusGloom/  Then you will want the file matching your server.

For the extra 2 directories provided this is for server owners running my mod Dis_OilRig and the NEAF_Quarantine_Z

mpmissions/Expansion.ChernarusPlusGloom/ This is including Loot spawns on my oil rig.
mpmissions/dayzOffline.Cheraruplus/ This is including loot spawns on my oil rig.

// ALL YOU NEED TO DO IS OPEN THE CORRECT DIR.  Copy the Mapgrouppus.xml 
// Paste this into your server folder: Mpmissions/XXXXXXXXXXXX/  
// In the same folder as your init.c  db  env storage_1 etc  you will see the original mapgrouppus.xml at the bottom.
// Feel free to take a copy of the original just incase you wipe / remove mods.

NOTE:

I cannot account for personal server setups - custom mapobjects IE: Further custom builds from the steam workshop
Or something you may have added yourself.

Learn how to do it yourself if you need further adjustments  to your personal server setup here:
https://community.bistudio.com/wiki/DayZ:Central_Economy_setup_for_custom_terrains

If you need help with or advise, let me know.

// PLEASE REMEMBER!!!
A) LIKE/AWARD MY MODS
B) LEAVE A POSATIVE COMMENT ON MY MOD
C) DONATE IF YOU FEEL ITS DESERVED >> https://paypal.me/bb2k20/

THANKS ENJOY!!

