///////////////////////////////////////////////////////////
//                 ZOMBIE SPAWN UPDATES READ ALL 		 //
///////////////////////////////////////////////////////////

Real easy to accomplish.
Switch off your server.

1) go to: YOUR SERVER 
mpmissions/XXXYOUR SERVERXXX as said with the loot readme 
dayzOffline.ChernarusPlus 
Expansion.ChernaruPlus 
Expansion.ChernarusPlusGloom 

2) Inside this Directory you will see have access to directories like: db, env, storage_1  init.c etc

3) go into: env DIR
4) Find: zombie_territories.xml   (Make a copy incase you mess up)

5) Inside my mod @Dis_NEAF_Quarantine_Z/Zombie spawns you have QZone_Z_zombie_Territories.xml
   open it with notepad++

6) Copy Each section into its relevant catagory EG: ARMY = ARMY = SOLITUDE = SOLITUTE.

7) Copy from

<!-- XXX -->
TO THE END OF THE CATAGORY IN MY FILE all zombie locations and zombie types. 
<!-- XXX -->

EXMAPLE: THIS IS CORRECT

	<!-- NEAF_Quarantine_Z InfectedReligion ONLY -->	
		<zone name="InfectedReligious" smin="0" smax="0" dmin="1" dmax="1" x="12356.00" z="12512.79" r="40"/>
	<!-- NEAF_Quarantine_Z ONLY InfectedReligion -->
	
//DO NOT COPY Example: THIS IS WRONG

    <territory color="1291845632">
	<!-- NEAF_Quarantine_Z InfectedReligiion ONLY -->	
		<zone name="InfectedReligious" smin="0" smax="0" dmin="1" dmax="1" x="12356.00" z="12512.79" r="40"/>
	<!-- NEAF_Quarantine_Z ONLY InfectedReligiion -->
    </territory>
	
8) Once you have the content you need. Locate: Example: InfectedReligion section in your original servers: zombie_Territories.xml
   Make a space like this:

EXAMPLE: 

    <territory color="1291845632">
// --> THIS SPACE
		<zone name="InfectedReligious" smin="0" smax="0" dmin="1" dmax="1" x="6.00" z="2.79" r="40"/>
		<zone name="InfectedReligious" smin="0" smax="0" dmin="1" dmax="1" x="6.00" z="2.79" r="40"/>
		<zone name="InfectedReligious" smin="0" smax="0" dmin="1" dmax="1" x="6.00" z="2.79" r="40"/>
		<zone name="InfectedReligious" smin="0" smax="0" dmin="1" dmax="1" x="6.00" z="2.79" r="40"/>
		<zone name="InfectedReligious" smin="0" smax="0" dmin="1" dmax="1" x="6.00" z="2.79" r="40"/>
    </territory>
	
9) Then Paste each section from my File to there file. 

EXAMPE: THIS IS CORRECT AND COMPLETE

    <territory color="1291845632">
	<!-- NEAF_Quarantine_Z InfectedReligion ONLY -->	
		<zone name="InfectedReligious" smin="0" smax="0" dmin="1" dmax="1" x="12356.00" z="12512.79" r="40"/>
	<!-- NEAF_Quarantine_Z ONLY InfectedReligion -->
		<zone name="InfectedReligious" smin="0" smax="0" dmin="1" dmax="1" x="6.00" z="2.79" r="40"/>
		<zone name="InfectedReligious" smin="0" smax="0" dmin="1" dmax="1" x="6.00" z="2.79" r="40"/>
		<zone name="InfectedReligious" smin="0" smax="0" dmin="1" dmax="1" x="6.00" z="2.79" r="40"/>
		<zone name="InfectedReligious" smin="0" smax="0" dmin="1" dmax="1" x="6.00" z="2.79" r="40"/>
		<zone name="InfectedReligious" smin="0" smax="0" dmin="1" dmax="1" x="6.00" z="2.79" r="40"/>
    </territory>
	
10) Do this for each and every type from my file, save it on your server. close it, boot the server and test.

If you need help with or advise, let me know.

// PLEASE REMEMBER!!!
A) LIKE/AWARD MY MODS
B) LEAVE A POSATIVE COMMENT ON MY MOD
C) DONATE IF YOU FEEL ITS DESERVED >> https://paypal.me/bb2k20/

THANKS ENJOY!!

