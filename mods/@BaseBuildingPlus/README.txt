INSTALLATION:

Inside the info folder you will find the example files that need adding to you server...



Add the code from types.xml to the types.xml in your mission folder. More info here:
https://github.com/FelixForesight2020/BaseBuildingPlus/wiki/Merging-types.xml



BBP_Settings.json should be created automatically in your server profile folder when you install BaseBuildingPlus. Open it up to change the following settings if you choose to.

    "g_BBPDisableCraftVanillaFence": 0,             0 = NO 1 = YES
    "g_BBPDisableCraftVanillaWatchtower": 0,        0 = NO 1 = YES
    "g_BBPRequireLandClaim": 0,			            0 = NO 1 = YES
    "g_BBPLandClaimBlockDismantle": 0,		        0 = NO 1 = YES
    "g_BBPLandClaimRadius": 30, 			        VALUES CAN BE BETWEEN 0 - 1000 (meters)
    "g_BBPAdminLandClaimRadius": 100, 		        VALUES CAN BE BETWEEN 0 - 1000 (meters)
    "g_BBPFloatingPlacement": 0, 			        0 = OFF 1 = ON
    "g_BBPAdvanceRotation": 0, 			            0 = OFF 1 = ON
    "g_BBPInventoryToggle": 0, 			            0 = OFF 1 = ON
    "g_BBPBarbedWireRemoveOutside": 1, 		        0 = NO 1 = YES
    "g_BBPSetInfiniteLifetime": 1, 			        0 = NO 1 = YES 
    "g_BBPEnableTimeDestructionMod": 0, 		    0 = NO 1 = YES
    "g_BBPRaidOnlyDoors": 0, 			            0 = NO 1 = YES
    "g_BBPTier1RaidTime": 30, 			            TIME TO RAID T1 (seconds)
    "g_BBPTier2RaidTime": 60, 			            TIME TO RAID T2 (seconds)
    "g_BBPTier3RaidTime": 120, 			            TIME TO RAID T3 (seconds)
    "g_BBPTier1RaidToolDamage": 25, 		        DAMAGE TO T1 RAID TOOL
    "g_BBPTier2RaidToolDamage": 50, 		        DAMAGE TO T2 RAID TOOL
    "g_BBPTier3RaidToolDamage": 1000, 		        DAMAGE TO T3 RAID TOOL
    "g_BBPTier1RaidTools": [],			            ADD OR REMOVE T1 RAID TOOLS
    "g_BBPTier2RaidTools": [],			            ADD OR REMOVE T2 RAID TOOLS
    "g_BBPTier3RaidTools": [],			            ADD OR REMOVE T3 RAID TOOLS
    "g_BBPCementMixerTime": 252, 			        VALUES CAN BE BETWEEN 0 - 1000 (seconds)
    "g_CementMixerLocations": []			        ADD OR REMOVE CEMENT MIXER LOCATIONS

More information can be found here:
https://github.com/FelixForesight2020/BaseBuildingPlus/wiki/Server-Configs



BreachingCharge1.0.json is included for reference, this file should be automatically created when you install Breachingcharge mod:
https://steamcommunity.com/sharedfiles/filedetails/?id=1920610398



If you are using Trader mod, add the code from TraderConfig.txt to the one in your server profile folder.
https://steamcommunity.com/sharedfiles/filedetails/?id=1590841260




DONATIONS:
This mod is completely free to use. If you want to contribute towards the future development you can do so by donating via PayPal or our official Patreon page. Any donations are much appreciated, thanks :)
PayPal Donations[www.paypal.com]
https://www.paypal.com/donate/?cmd=_s-xclick&hosted_button_id=G369MZJVJW82W
Patreon Donations[www.patreon.com]
https://www.patreon.com/dayzbbp



	
	join the discord if you need any assistance 
	https://discord.gg/GFeTV7H
